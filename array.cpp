#include "array.h"
#include <cstdlib>
#include <cmath>

using namespace std;

unsigned long long Array::m_count = 0;

unsigned long long Array::m_memory(unsigned long long size) {
	unsigned long long i = 0;
	while (size * sizeof(double) > (unsigned long long)pow(2,i)) {
		i++;
	}
	cout << "Size of elem and memory: " << size << ", " << size * sizeof(double) << ", size of pow: " << (unsigned long long)pow(2,i) << endl;
	return (unsigned long long)pow(2,i);
}


void Array::m_init(unsigned long long size, double init) {
	m_arr = (double *)malloc(Array::m_memory(size));
	for (unsigned long long i = 0; i < size; i++) {
		m_arr[i] = init;
	}
	m_arrSize = size;
	m_count += m_arrSize;
}

Array::Array() {
	Array::m_init(10, 0.0);
}

Array::Array(unsigned long long size) {
	Array::m_init(size, 0.0);
}

Array::Array(unsigned long long size, double init) {
	Array::m_init(size, init);
}

//Array::Array(const Array &arrObj):
//	m_arrSize(arrObj.m_arrSize)
Array::Array(Array&& arrObj):
	m_arrSize(arrObj.m_arrSize)
{
	cout << "IN Array::Array(const Array &a)" << endl;
	cout << "\tm_arrSize: " << m_arrSize << endl;
	m_arr = arrObj.m_arr;
	// m_arr = (double *)malloc(Array::m_memory(arrObj.m_arrSize));
	// for (unsigned long long i = 0; i < arrObj.m_arrSize; i++)
	// 	m_arr[i] = arrObj.m_arr[i];
	m_count += arrObj.m_arrSize;
	arrObj.m_arr = nullptr;
	arrObj.m_arrSize = 0;
}

Array::~Array() {
	cout << "Array::~Array()" << endl;
	m_count -= m_arrSize;
	free(m_arr);
}

// Array& Array::operator=(const Array &source) {
Array& Array::operator=(Array&& source) {
	cout << "Array::operator=()" << endl;
	m_arr = source.m_arr;
	// m_arr = (double *)malloc(Array::m_memory(source.m_arrSize));
	// for (unsigned long long i = 0; i < source.m_arrSize; i++)
	// 	m_arr[i] = source.m_arr[i];
	m_count = m_count - m_arrSize + source.m_arrSize;
	m_arrSize = source.m_arrSize;
	source.m_arr = nullptr;
	source.m_arrSize = 0;
	return *this;
}

Array Array::operator+(const Array& right) const {
	cout << "Array::operator+()" << endl;
	Array temp(this->m_arrSize + right.m_arrSize);

	for (unsigned long long i = 0; i < this->m_arrSize; i++)
		temp.m_arr[i] = this->m_arr[i];
	for (unsigned long long i = 0; i < right.m_arrSize; i++)
		temp.m_arr[i + this->m_arrSize] = right.m_arr[i];

	temp.m_arrSize = this->m_arrSize + right.m_arrSize;

	return temp;
}

double & Array::operator[](unsigned long long n) {
	return m_arr[n];
}

std::ostream& operator<<(std::ostream& s, const Array& source) {
	for (unsigned long long i = 0; i < source.m_arrSize-1; i++)
		s << source.m_arr[i] << " ";
	s << source.m_arr[source.m_arrSize-1] << endl;
	return s;
}

double & Array::at(unsigned long long n) {
	if (n <= m_arrSize)
		return m_arr[n];
	else {
		cout << "Индекс за пределами длины массива. Возвращение последнего элемента." << endl;
		return m_arr[m_arrSize-1];
	}
}

void Array::pushBack(double num) {
	if (Array::m_memory(m_arrSize + 1) > Array::m_memory(m_arrSize)) {
		m_arr = (double *)realloc(m_arr, Array::m_memory(m_arrSize + 1));
		m_arr[m_arrSize] = num;
		m_arrSize++;
		m_count++;
	}
	else {
		m_arr[m_arrSize] = num;
		m_arrSize++;
		m_count++;
	}
}

void Array::pushBack(const Array& source) {
	if (Array::m_memory(m_arrSize + source.m_arrSize) > Array::m_memory(m_arrSize)) {
		m_arr = (double *)realloc(m_arr, Array::m_memory(m_arrSize + source.m_arrSize));
		for (unsigned long long i = 0; i < source.m_arrSize; i++) {
			m_arr[i + m_arrSize] = source.m_arr[i];
		}
		m_arrSize += source.m_arrSize;
		m_count += source.m_arrSize;
	}
	else {
		for (unsigned long long i = 0; i < source.m_arrSize; i++) {
			m_arr[i + m_arrSize] = source.m_arr[i];
		}
		m_arrSize += source.m_arrSize;
		m_count += source.m_arrSize;
	}
}

void Array::popBack() {
	if (Array::m_memory(m_arrSize - 1) < Array::m_memory(m_arrSize)) {
		cout << "TAKE YOUR FUCKING ELEMENT: " << m_arr[m_arrSize-1] << endl;
		m_arr = (double *)realloc(m_arr, Array::m_memory(m_arrSize - 1));
		m_arrSize--;
		m_count--;
	}
	else {
		cout << "TAKE YOUR FUCKING ELEMENT: " << m_arr[m_arrSize-1] << endl;
		m_arrSize--;
		m_count--;
	}
}

double Array::getElement(unsigned long long n) const {
	return m_arr[n];
}

void Array::setElement(unsigned long long n, double value) {
	m_arr[n] = value;
}

unsigned long long Array::getSize() const {
	return m_arrSize;
}

unsigned long long Array::getCount() {
	return m_count;
}
