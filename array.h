#include <iostream>
#pragma once

class Array {
	double *m_arr;
	unsigned long long m_arrSize;
	void m_init(unsigned long long size, double init);
	unsigned long long m_memory(unsigned long long size);
	static unsigned long long m_count;

public:
	Array();
	Array(unsigned long long size);
	Array(unsigned long long size, double init);
	Array(Array&& arrObj);
	// Array(const Array &arrObj);
	~Array();

	Array &  operator=(Array&& source);
	// Array &  operator=(const Array &source);
	Array    operator+(const Array& right) const;
	double & operator[](unsigned long long n);

	friend std::ostream& operator<<(std::ostream& s, const Array& source);

	void pushBack(double num);
	void pushBack(const Array& source);
	void popBack();

	double &           at(unsigned long long n);
	double			   getElement(unsigned long long n) const;
	unsigned long long getSize() const;
	void               setElement(unsigned long long n, double value);

	static unsigned long long getCount();
};
