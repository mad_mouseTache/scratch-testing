#include <iostream>
#include <cmath>
#include "array.h"

using namespace std;

void printArray(const Array &a){
	for(size_t i = 0; i < a.getSize(); i++){
		cout << a.getElement(i) << " ";
	}
}

Array f(Array a){
	return a;
}

Array sum(const Array a, const Array b){
	Array temp = a + b;
	return temp;
}

int main(){

	{
		Array a(10, 5), b(5, 10), c;
		c = a + b;
		cout << "Amount of elements: " << Array::getCount() << endl;

		cout << c;
		c.popBack();
		cout << c;
		cout << "Amount of elements: " << Array::getCount() << endl;
		
		// printArray(c);
		// cout << endl << endl;

		// cout << "Before f()" << endl;
		// c = f(c);
		// cout << "After f()" << endl << endl;

		// cout << "Before sum()" << endl;
		// c = sum(a, b);
		// cout << "After sum()" << endl << endl;
	}

	cout << "Amount of elements: " << Array::getCount() << endl;

	return 0;
}
